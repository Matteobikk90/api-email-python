# import os
# import smtplib
# # from flask import Flask, redirect, url_for, render_template, request
# # app = Flask(__name__)
# # from flask import Flask
# # app = Flask(__name__)


# # @app.route("/")
# # def hello():
# #     return "Hello World!"


# # Get environment variables
# EMAIL = os.environ.get('MY_EMAIL')
# PASS = os.environ.get('MY_PASS')

# sent_from = EMAIL
# to = ['matteo.soresini90@gmail.com']
# subject = 'OMG Super Important Message'
# body = 'Testing'

# email_text = """\
# From: %s
# To: %s
# Subject: %s

# %s
# """ % (sent_from, ", ".join(to), subject, body)

# try:
#     server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
#     server.ehlo()
#     server.login(EMAIL, PASS)
#     server.sendmail(sent_from, to, email_text)
#     server.close()

#     print('Email sent!')
# except:
#     print('Something went wrong...')

import os
import smtplib
from email.message import EmailMessage
from flask import Flask, flash, request, render_template, jsonify
from flask_cors import CORS


# # Get environment variables
EMAIL = os.environ.get('MY_EMAIL')
PASS = os.environ.get('MY_PASS')
SECRET_KEY_FLASK = os.environ.get("SECRET_KEY_FLASK")

app = Flask(__name__)
app.secret_key = SECRET_KEY_FLASK
# static_folder='static',
# template_folder='templates')

# port = int(os.environ.get("PORT", 5000))
CORS(app)


@app.route('/')
def index():
    return "<h1>Working</h1>"


@app.route('/subscribe', methods=['POST'])
def subscribe():
    jsonData = request.get_json()
    NEWSLETTER_EMAIL = jsonData['email']
    msg = EmailMessage()
    msg['Subject'] = """Nuova iscrizione newsletter - Gio' Gatto"""
    msg['From'] = EMAIL
    msg['To'] = ['matteo.soresini@hotmail.it']
    msg.add_alternative(f"""
        Email-Newsletter: {NEWSLETTER_EMAIL}
    """)

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(EMAIL, PASS)
        server.send_message(msg)
        server.close()
        # flash('Message sent successfully', 'success')
        return jsonify({'success': 'Registrazione alla newsletter avvenuta correttamente'})

    except:
        # print('Something went wrong...')
        # flash("Please enter a valid email address", 'error')
        return jsonify({'error': 'Errore durante la registrazione alla newsletter, riprovare'})


@app.route('/send_message', methods=['POST'])
def send_message():
    jsonData = request.get_json()
    CUSTOMER_NAME = jsonData['name']
    CUSTOMER_PROFESSION = jsonData['profession']
    CUSTOMER_PHONE = jsonData['phone']
    CUSTOMER_EMAIL = jsonData['email']
    CUSTOMER_MESSAGE = jsonData['message']

    msg = EmailMessage()
    msg['Subject'] = f'Richiesta informazioni da {CUSTOMER_NAME}'
    msg['From'] = EMAIL
    msg['To'] = ['matteo.soresini@hotmail.it']
    msg.add_alternative(f"""
        Nome: {CUSTOMER_NAME} \n
        Professione: {CUSTOMER_PROFESSION} \n
        Tel :{CUSTOMER_PHONE} \n
        Email: {CUSTOMER_EMAIL} \n
        Messaggio: {CUSTOMER_MESSAGE}
    """)

    try:
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.ehlo()
        server.starttls()
        server.login(EMAIL, PASS)
        server.send_message(msg)
        server.close()
        # print('Email sent!')
        # flash('Message sent successfully', 'success')

        return jsonify({'success': 'Email inviata correttamente'})

    except:
        # print('Something went wrong...')
        # flash("Please enter a valid email address", 'error')
        return jsonify({'error': 'Invio non riuscito, riprovare'})


# if __name__ == '__main__':
#     # Threaded option to enable multiple instances for multiple user access support
#     app.run(threaded=True, port=5000)
